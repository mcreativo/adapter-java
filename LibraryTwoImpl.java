public class LibraryTwoImpl implements LibraryTwo
{
    @Override
    public void doA()
    {
        System.out.println("Doing A part");
    }

    @Override
    public void doB()
    {
        System.out.println("Doing B part");
    }
}
